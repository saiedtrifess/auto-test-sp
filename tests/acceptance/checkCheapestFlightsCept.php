<?php

class Flights
{
	private $strategy = NULL;

	public function __construct($strategy_ind_id)
	{
		switch ($strategy_ind_id)
		{
			case "Cheap":
				$this->strategy = new StrategyCheap();
			break;

			case "Quick":
				$this->strategy = new StrategyQuick();
			break;

		}
	}

	public function getFlightsPrice($flight)
	{
		return $this->strategy->printPrice($flight);
	}
}

interface StrategyInterface
{
	public function getPrice($flight);
}



// $I = new AcceptanceTester($scenario);
// $I->wantTo('perform actions and see result');
// $I->amOnPage('/');
// $I->see('Log in');

// $I->wait(200);
// $I->see('//*[@id="simplemodal-data"]/div[3]/div[2]/a');

// $I->see('Search');
// $I->see(['class' => 'twitter-typeahead']);


