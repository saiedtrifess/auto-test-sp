<?php

class FlightsOptions 
{
	private $option = NULL;
	public function __construct($strategy_ind_id)
	{
		switch ($strategy_ind_id) {

		case "cheap":
			$this->option = new OptionCheap();
		break;

		case "exp":
			$this->option = new  OptionExp();
		break;
		}
	}

	public function getPriceRequest($flight,array $price_in)
	{
		return $this->option->getPrice($flight,$price_in);
	}
}

interface FlightsInterface{
	public function getPrice($flight, array $price_in);
}
class OptionCheap implements FlightsInterface {
	public function getPrice ($flight, array $price_in)
	{
		$price = $flight->cheapeset($price_in);

		return $price;
	}
}

 class OptionExp implements FlightsInterface {
 	public function getPrice ($flight,array $price_in)
 	{
 		$price = $flight->expensive($price_in);
 		return $price;
 	}
 }

class Flights
{
	private $price;
	function cheapeset(array $prince_in)
	{
		$min = $prince_in[0];

		for ($i=0; $i<count($prince_in); $i++)
		{
			if ($prince_in[$i] < $min)
			{
				$min = $prince_in[$i];
			}
		}
		return $min;
	}
	function expensive(array $prince_in)
	{
		$max = $prince_in[0];

		for ($i=0; $i<count($prince_in); $i++)
		{
			if ($prince_in[$i] > $max)
			{
				$max = $prince_in[$i];
			}
		}
		return $max;
	}
} 
?>